use std::collections::HashMap;

const TILE_EDGE_SIZE: usize = 10;
// For test.
const GAME_EDGE_SIZE: usize = 3;
// For regular input.
// const GAME_EDGE_SIZE: usize = 12;

pub fn main() {
    // assert_eq!(part1(include_str!("test.txt")), 20899048083289);
    assert_eq!(part2(include_str!("test.txt")), 20899048083289);
    // assert_eq!(part1(include_str!("input.txt")), 13983397496713);
}

struct Tile {
    id: usize,
    grid: [[char; TILE_EDGE_SIZE]; TILE_EDGE_SIZE],
    t: usize,
    b: usize,
    l: usize,
    r: usize,
}

impl Tile {
    fn rotate_right(&mut self) {
        let tmp = self.t;
        self.t = self.l;
        self.l = self.b;
        self.b = self.r;
        self.r = tmp;
        self.grid = transpose(self.grid);
        self.grid = fliph(self.grid);
    }
    fn flipv(&mut self) {
        let tmp = self.t;
        self.t = self.b;
        self.b = tmp;
        self.grid = flipv(self.grid);
    }
    fn fliph(&mut self) {
        let tmp = self.l;
        self.l = self.r;
        self.r = tmp;
        self.grid = fliph(self.grid);
    }
}


impl std::fmt::Display for Tile {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "ID:{}  t:{} l:{} b:{}  r:{}", self.id, self.t, self.l, self.b, self.r)
    }
}

impl PartialEq for Tile {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
    }
}

fn transpose(graph: [[char; TILE_EDGE_SIZE]; TILE_EDGE_SIZE] )
    -> [[char; TILE_EDGE_SIZE]; TILE_EDGE_SIZE]
{
    let mut out =  [['.'; TILE_EDGE_SIZE]; TILE_EDGE_SIZE];
    for i in 0..TILE_EDGE_SIZE{
        for j in 0..TILE_EDGE_SIZE{
            out[i][j] = graph[j][i];
        }
    }
    out
}

fn flipv(graph: [[char; TILE_EDGE_SIZE]; TILE_EDGE_SIZE] )
    -> [[char; TILE_EDGE_SIZE]; TILE_EDGE_SIZE]
{
    let mut ret = [['.'; TILE_EDGE_SIZE]; TILE_EDGE_SIZE];
    for (row_idx, _) in graph.iter().enumerate() {
        ret[graph.len() - row_idx -1] = graph[row_idx];
    }
    ret
}

fn fliph(graph: [[char; TILE_EDGE_SIZE]; TILE_EDGE_SIZE] )
    -> [[char; TILE_EDGE_SIZE]; TILE_EDGE_SIZE]
{
    let mut ret = [['.'; TILE_EDGE_SIZE]; TILE_EDGE_SIZE];
    for (row_idx, row) in graph.iter().enumerate() {
        for (col_idx, col) in row.iter().enumerate() {
            ret[row_idx][row.len() - col_idx -1] = *col;
        }
    }
    ret
}

fn load_input(input: &str)
    -> Vec<Tile>
{
    let mut ret: Vec<Tile>  = Vec::new();
    for tile_input_lines in input.split("\n\n") {
        let mut tile_grid = [['.'; TILE_EDGE_SIZE]; TILE_EDGE_SIZE];
        let mut line_iter = tile_input_lines.split("\n");
        let first_line = line_iter.next().unwrap();
        let tile_number = first_line[5..9].parse::<usize>().unwrap();
        for (row_ctr, line_of_grid) in line_iter.enumerate() {
            for (col_ctr, char_of_grid) in line_of_grid.chars().enumerate() {
                tile_grid[row_ctr][col_ctr] = char_of_grid;
            }
        }
        let (t, b, l, r) = convert_edge_to_number(&tile_grid);
        let tile = Tile {
                id: tile_number,
                grid: tile_grid,
                t:t, b:b, l:l, r:r,
            };
        // println!("Initiated tile {}", tile);
        ret.push(tile);
    }
    ret
}

fn convert_edge_to_number(tile_shape: &[[char; TILE_EDGE_SIZE]; TILE_EDGE_SIZE])
    -> (usize, usize, usize, usize)
{
    fn convert_arr_to_max_int(arr: &[char; TILE_EDGE_SIZE]) -> usize {
        // println!("Arr {}", &arr.iter().collect::<String>());
        let ret_arr = arr
            .iter()
            .map(|c| {
                if c == &'.' {
                    '0'
                } else {
                    '1'
                }
            })
            .collect::<String>();
        usize::max(
            usize::from_str_radix(&ret_arr, 2).unwrap(),
            usize::from_str_radix(&ret_arr.chars().rev().collect::<String>(), 2).unwrap()
        )
        // println!("Converted edge {} to number {}", ret_arr, max);
    }
    let top = tile_shape[0];
    let bot = tile_shape[tile_shape.len()-1];
    let mut left = ['.'; TILE_EDGE_SIZE];
    for (row_idx, row) in tile_shape.iter().enumerate() {
        left[row_idx] = row[0];
    }
    let mut right = ['.'; TILE_EDGE_SIZE];
    for (row_idx, row) in tile_shape.iter().enumerate() {
        right[row_idx] = row[row.len()-1];
    }

    let top_int = convert_arr_to_max_int(&top);
    let bot_int = convert_arr_to_max_int(&bot);
    let left_int = convert_arr_to_max_int(&left);
    let right_int = convert_arr_to_max_int(&right);
    (top_int, bot_int, left_int, right_int)
}

fn locate_top_left_tile(all_tiles: &Vec<Tile>) -> (usize,  Vec<usize>) {
    let mut edge_to_tile_map: HashMap<usize, Vec<&Tile>> = HashMap::new();
    for tile in all_tiles {
        for &edge in &[tile.t, tile.b, tile.l, tile.r] {
            edge_to_tile_map.entry(edge).or_insert(Vec::new()).push(tile);
        }
    }
    let mut edge_and_corner_tiles: Vec<&Tile> = Vec::new();
    let mut exposed_edges: Vec<usize> = Vec::new();
    for (edge_id, applicable_tiles) in edge_to_tile_map.iter() {
        if applicable_tiles.len() == 1 {
            edge_and_corner_tiles.push(applicable_tiles.get(0).unwrap());
            exposed_edges.push(*edge_id);
        }
    }
    let mut edge_tiles: Vec<&Tile> = Vec::new();
    let mut corner_tiles: Vec<&Tile> = Vec::new();
    for tile in edge_and_corner_tiles {
        if edge_tiles.contains(&tile) {
            corner_tiles.push(tile);
            let tile_index = edge_tiles.iter().position(|x| *x == tile).unwrap();
            edge_tiles.remove(tile_index);
        } else {
            edge_tiles.push(tile);
        }
    }
    for tile in &edge_tiles{
        println!("edge_tiles: {}", tile);
    }
    for tile in &corner_tiles{
        println!("corner_tiles: {}", tile);
    }
    let top_left_tile = corner_tiles.get(0).unwrap();
    (all_tiles.iter().position(|x| &x == top_left_tile).unwrap(), exposed_edges)
}

/*
fn part1(input: &str) -> (usize) {
    let mut all_tiles: Vec<Tile> = load_input(input);
    for tile in &all_tiles{
        println!("TIle: {}", tile);
    }
    let (corner_tiles, edge_tiles) = locate_corner_and_edge_tiles(&mut all_tiles);
    let mut ret = 1;
    for tile in &corner_tiles {
        ret *= tile.id;
    }
    ret
}
*/

fn find_tile_idx_with_edge(edge:&usize, all_tiles: &Vec<Tile>) -> usize {
    let mut ret:usize = 0;
    for tile_candidate_idx in 0..all_tiles.len() {
        let tile_candidate = all_tiles.get(tile_candidate_idx).unwrap();
        if [tile_candidate.r,
            tile_candidate.t,
            tile_candidate.l,
            tile_candidate.b].contains(&edge)
        {
            ret = tile_candidate_idx;
            break;
        }
    }
    println!("Identified tile: {}", all_tiles.get(ret).unwrap());
    ret
}

fn part2(input: &str) -> usize {
    let mut all_tiles: Vec<Tile> = load_input(input);
    for tile in &all_tiles {
        println!("TIle: {}", tile);
    }
    let (top_left_tile_idx, exposed_edges) = locate_top_left_tile(&all_tiles);

    let top_left_tile = all_tiles.get(top_left_tile_idx).unwrap();
    println!("Top left tile {}", &top_left_tile);

    while !(exposed_edges.contains(&top_left_tile.l)) {
        println!("Rotating right");
        top_left_tile.rotate_right();
    }
    if !(exposed_edges.contains(&top_left_tile.t)) {
        println!("Rotating right once more.");
        top_left_tile.rotate_right();
    }
    assert!(exposed_edges.contains(&top_left_tile.l));
    assert!(exposed_edges.contains(&top_left_tile.t));
    println!("Top left tile {}", &top_left_tile);

    let mut board: Vec<Vec<Tile>> = Vec::new();
    let mut anchor_tile_idx = top_left_tile_idx;
    while board.len() < GAME_EDGE_SIZE {
        let mut board_row: Vec<Tile> = Vec::new();
        let current_row_anchor_tile = all_tiles.swap_remove(anchor_tile_idx);
        board_row.insert(0, current_row_anchor_tile);
        while board_row.len() < GAME_EDGE_SIZE {
            let left_most_tile = board_row.last().unwrap();
            let tile_candidate_idx = find_tile_idx_with_edge(&left_most_tile.l, &all_tiles);
            if tile_candidate_idx == 0 {
                panic!("Tile Candidate Index was zero");
            }
            let mut tile_candidate: Tile = all_tiles.swap_remove(tile_candidate_idx);
            while tile_candidate.l != current_row_anchor_tile.r {
                tile_candidate.rotate_right();
                println!("Rotating candidate tile: {}", tile_candidate);
            }
            println!("Adding to vector: {}", tile_candidate);
            board_row.push(
                all_tiles.swap_remove(tile_candidate_idx)
            );
            break;
        }
        board.push(board_row);
        if all_tiles.len() <= 0 {
            println!("All tiles is depopulated");
            break;
        }
        let next_row_anchor_tile_idx = find_tile_idx_with_edge(&board_row.get(0).unwrap().b, &all_tiles);
        let mut next_row_anchor_tile = all_tiles.swap_remove(next_row_anchor_tile_idx);
        while current_row_anchor_tile.b != next_row_anchor_tile.t {
            next_row_anchor_tile.rotate_right();
            println!("Rotating candidate anchor tile: {}", next_row_anchor_tile);
        }
        anchor_tile_idx = next_row_anchor_tile_idx;
    }
    let coner_ids: Vec<usize> = identify_corner_tile_ids(board);
    let mut prod = 1;
    for id in coner_ids {
        prod *= id;
    }
    prod
}

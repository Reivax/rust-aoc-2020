use std::collections::HashMap;

static INACTIVE:char = '.';
static ACTIVE:char = '#';

struct Board {
    grid: HashMap<(i8, i8, i8), char>, //State of the world
    nx:usize, // Initial x size.
    ny:usize, // Initial y size.
}

pub fn main() {
    assert_eq!(part1(include_str!("test.txt"), 0), 5);
    assert_eq!(part1(include_str!("test.txt"), 6), 112);
    assert_eq!(part1(include_str!("test.txt"), 6), 259);
}

fn count_active_neighbors(board: &Board, x:i8, y:i8, z:i8)
    -> usize {
    let mut ctr:usize = 0;
    for dz in vec![-1,0,1] {
        for dy in vec![-1,0,1] {
            for dx in vec![-1,0,1] {
                if (dz == dy) == (dx == 0) {
                    continue;
                }
                else if board.grid.get(&(z+dz, y+dy, x+dx)).unwrap_or(&INACTIVE) == &ACTIVE {
                    ctr += 1;
                }
            }
        }
    }
    ctr
}

fn advance_state(current_board: &Board, generation:i8) -> HashMap<(i8, i8, i8), char> {
    let mut next_grid = HashMap::new();

    for z in (-generation-1)..=(generation+1) as i8 {
        for y in (-generation - 1)..=(current_board.ny as i8 + generation) as i8 {
            for x in (-generation - 1)..=(current_board.nx as i8 + generation) as i8 {
                let state = current_board.grid.get(&(z,y,x)).unwrap_or(&INACTIVE);
                let neighbor_count = count_active_neighbors(current_board, x, y, z);
                // println!("State for Gen {}, {} {} {} - {}  Neighbors: {}", generation, z, y, x, state, neighbor_count);
                if state == &ACTIVE {
                    if neighbor_count == 2 || neighbor_count == 3 {
                        next_grid.insert((z, y, x), ACTIVE);
                    } else {
                        //next_grid.insert((z, y, x), INACTIVE);
                    }
                }
                else if state == &INACTIVE {
                    if  neighbor_count == 3 {
                        next_grid.insert((z, y, x), ACTIVE);
                    } else {
                        //next_grid.insert((z, y, x), INACTIVE);
                    }
                }
            }
        }
    }
    next_grid
}

fn part1(input: &str, turn_limit:usize) -> usize {
    let mut current_board = load_into_board(input);
    for generation in 1..=turn_limit as i8 {
        current_board.grid = advance_state(&current_board, generation);
    }
    count_active(&current_board)
}

fn load_into_board(input: &str) -> Board {
    let mut grid = HashMap::new();
    for (row, line) in input.lines().enumerate() {
        for (col, char) in line.chars().enumerate() {
            grid.insert((0 as i8, row as i8, col as i8), char);
        }
    }
    Board{
        grid: grid,
        ny: input.lines().count(),
        nx: input.lines().nth(0).unwrap().chars().count(),
    }
}

fn count_active(board: &Board) -> usize {
    let mut ctr:usize = 0;
    for key in board.grid.keys() {
        if board.grid.get(key).unwrap_or(&INACTIVE) == &ACTIVE {
            ctr += 1
        }
    }
    ctr
}

use std::collections::HashMap;

static INACTIVE:char = '.';
static ACTIVE:char = '#';

pub fn main() {
    assert_eq!(part2(include_str!("test.txt"), 0), 5);
    assert_eq!(part2(include_str!("test.txt"), 1), 29);
    assert_eq!(part2(include_str!("test.txt"), 2), 60);
    assert_eq!(part2(include_str!("test.txt"), 3), 320);
    assert_eq!(part2(include_str!("test.txt"), 4), 188);
    assert_eq!(part2(include_str!("test.txt"), 5), 1056);
    assert_eq!(part2(include_str!("test.txt"), 6), 848);
    assert_eq!(part2(include_str!("input.txt"), 6), 2228);
}

fn tag_neighbors(neighbor_count_board: &mut HashMap<(isize, isize, isize, isize), usize>, w:isize, x:isize, y:isize, z:isize) {
    for dz in vec![-1,0,1] {
        for dy in vec![-1,0,1] {
            for dx in vec![-1,0,1] {
                for dw in vec![-1, 0, 1] {
                    if (dx, dy, dz, dw) == (0, 0, 0, 0) {
                        continue;
                    } else {
                        *neighbor_count_board
                            .entry((z+dz, y+dy, x+dx, w+dw))
                            .or_insert(0)
                            += 1;
                    }
                }
            }
        }
    }
}

fn advance_state(current_board: &HashMap<(isize, isize, isize, isize), char>)
                 -> HashMap<(isize, isize, isize, isize), char>
{
    let mut neighbor_count_board: HashMap<(isize, isize, isize, isize), usize> = HashMap::new();
    let mut next_board:HashMap<(isize, isize, isize, isize), char> = HashMap::new();

    // For every active cell in the space, add one to every one of its neighbors.
    for &(z, y, x, w) in current_board.keys() {
        tag_neighbors(&mut neighbor_count_board, w, x, y, z);
    }
    // For every integer value in the neighbor map, check its value and its current state.
    for ((z, y, x, w), neighbor_count) in neighbor_count_board {
        let current_state = *current_board.get(&(z, y, x, w)).unwrap_or(&INACTIVE);

        if (x, y, z, w)==(0, 0, 0, 0) {
            //println!("Central value and neighbors: {} {}", current_state, neighbor_count)
        }
        if current_state == INACTIVE && neighbor_count == 3 {
            next_board.insert((z, y, x, w), ACTIVE);
        } else if current_state == ACTIVE && ( neighbor_count == 3 || neighbor_count == 2 ) {
            next_board.insert((z, y, x, w), ACTIVE);
        }
    }
    next_board
}

fn part2(input: &str, turn_limit:usize) -> usize {
    let mut current_board: HashMap<(isize, isize, isize, isize), char> = load_into_board(input);
    // quick_print_board(&current_board, 0);
    for generation in 1..=turn_limit as isize {
        current_board = advance_state(&current_board);
        // quick_print_board(&current_board, generation)
    }
    count_active(&current_board)
}

fn load_into_board(input: &str) -> HashMap<(isize, isize, isize, isize), char> {
    let mut grid_active = HashMap::new();
    for (row, line) in input.lines().enumerate() {
        for (col, char) in line.chars().enumerate() {
            if char == ACTIVE {
                grid_active.insert((0 as isize, 0 as isize, row as isize, col as isize), ACTIVE);
            }
        }
    }
    grid_active
}

fn count_active(board: &HashMap<(isize, isize, isize, isize), char>) -> usize {
    let mut ctr:usize = 0;
    for (k, v) in board {
        if v == &ACTIVE {
            ctr += 1
        }
    }
    ctr
}

fn quick_print_board(board: &HashMap<(isize, isize, isize, isize), char>, generation:isize) {
    println!("Generation {}", generation);
    for ((z, y, x, w), value) in board {
        println!("Entry G{} Z{} Y{} X{} W{} {}", generation, z, y, x, w, value);
    }
}

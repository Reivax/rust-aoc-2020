use std::collections::HashMap;

pub fn main() {
    assert_eq!(determine_value_at_turn(vec![0,3,6], 9), 4);
    assert_eq!(determine_value_at_turn(vec![0,3,6], 10), 0);
    assert_eq!(determine_value_at_turn(vec![0,3,6], 2020), 436);
    assert_eq!(determine_value_at_turn(vec![5,1,9,18,13,8,0], 2020), 376);
    //assert_eq!(determine_value_at_turn(vec![5,1,9,18,13,8,0], 30_000_000), 323780);
}

fn determine_value_at_turn(initial_state: Vec<u32>, target_turn:u32) -> u32 {
    let mut last_spoken_dict = HashMap::new();
    let mut spoken_now: u32 = 0;
    for (&val, turn) in initial_state.iter().zip(1..) {
        last_spoken_dict.insert(val, turn);
        spoken_now = val;
    }
    let mut last_spoken:u32;
    for turn in (initial_state.len() as u32)..target_turn {
        // if turn%100000==0 {
        //     println!("Turn {}", turn);
        // }
        last_spoken = spoken_now;
        spoken_now = turn - last_spoken_dict.get(&last_spoken).unwrap_or(&turn);
        last_spoken_dict.insert(last_spoken, turn);
    }
    spoken_now
}

pub fn main() {
    effort_1()
}

fn effort_1() {
    let product: usize = vec! [(1,1), (3,1), (5,1), (7,1), (1,2)]
        .iter()
        .map(|&(right, down)| {
            include_str!("input.txt")
            //include_str!("input_chase.txt")
                .lines()
                .step_by(down)
                .enumerate()
                //.skip(1)
                .fold(0, |acc, (idx, line)| {
                    let i = (idx * right) % line.chars().count();
                    acc + (line.chars().nth(i).unwrap() == '#') as usize
                })
        })
        .product();
    println!("Product for full runs: {}", product);
}

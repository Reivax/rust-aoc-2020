pub fn main() {
    //pythonily()
    rusticly()
}

fn rusticly() {
    let input = include_str!("input.txt");
    let correct = input
        .lines()
        .map(|line| parse_line(line))
        .filter(|(l, r, c, pwd)| is_valid(*l, *r, c, pwd))
        .count();
    println!("Correct {}", correct)
}

fn is_valid(l:i32, r:i32, targetc:&char, pwd:&str) -> bool{
    let count = pwd
        .chars()
        .filter(|char| char == targetc)
        .count() as i32;
    count >= l && count <= r
}

fn pythonily() {
    let input = include_str!("input.txt");
    let mut correct = 0;
    for line in input.lines() {
        let (minct, maxct, targetc, pwd) = parse_line(line);
        // println!("Min {}  Max {}  Target: {}   Pwd: {}", minct, maxct, targetc, pwd);
        let ct = pwd.matches(targetc).count();
        // println!("Count: {}", ct);
        let cti: i32 = ct as i32;
        if (minct <= cti) && (cti <= maxct) {
            correct = correct + 1;
        }
    }
    println!("Correct: {}", correct)
}

fn parse_line(line: &str) -> (i32, i32, char, &str) {
    // Split on the dash
    let parts: Vec<&str> = line.split("-").collect();
    let minct: i32 = parts[0].parse().unwrap();
    // Split on spaces, three parts returned.
    let parts: Vec<&str> = parts[1].split(" ").collect();
    // First part is int.
    let maxct: i32 = parts[0].parse().unwrap();
    // Second part is character, with a colon.
    let targetc: &str = &parts[1][0..1];
    let char_vec: Vec<char> = targetc.chars().collect();
    let targetc = char_vec[0];
    // Third part is given password.
    let pwd = parts[2];
    (minct, maxct, targetc, pwd)
}

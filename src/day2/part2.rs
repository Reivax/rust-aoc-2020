pub fn main() {
    //pythonily()
    rusticly()
}

fn rusticly() {
    let input = include_str!("input.txt");
    let correct = input
        .lines()
        .map(|line| parse_line(line))
        //.filter(|(l, r, c, pwd)| is_valid(*l, *r, c, pwd))
        .filter(|(l, r, c, pwd)|
            pwd
                .chars()
                .enumerate()
                .filter(|(idx, char)| *idx == *l as usize || *idx == *r as usize)
                .filter(|(idx, char)| char == c)
                .count() == 1
        )
        .count();
    println!("Correct {}", correct)
}

fn is_valid(l:i32, r:i32, targetc:&char, pwd:&str) -> bool{
    let l = l as usize;
    let r = r as usize;
    pwd
        .chars()
        .enumerate()
        .filter(|(idx, char)| *idx == l || *idx == r)
        .filter(|(idx, char)| char == targetc)
        .count() == 1
}

fn pythonily() {
    let input = include_str!("input.txt");
    let mut correct = 0;
    for line in input.lines() {
        let (mut minct, mut maxct, targetc, pwd) = parse_line(line);
        let minct = minct as usize;
        let maxct = maxct as usize;
        let pwd_vec: Vec<char> = pwd.chars().collect();
        if (pwd_vec[minct] == targetc) ^ (pwd_vec[maxct] == targetc) {
            correct = correct + 1
        }
    }
    println!("Correct: {}", correct)
}

fn parse_line(line: &str) -> (i32, i32, char, &str) {
    // Split on the dash
    let parts: Vec<&str> = line.split("-").collect();
    let minct: i32 = parts[0].parse().unwrap();
    // Split on spaces, three parts returned.
    let parts: Vec<&str> = parts[1].split(" ").collect();
    // First part is int.
    let maxct: i32 = parts[0].parse().unwrap();
    // Second part is character, with a colon.
    let targetc: &str = &parts[1][0..1];
    let char_vec: Vec<char> = targetc.chars().collect();
    let targetc = char_vec[0];
    // Third part is given password.
    let pwd = parts[2];
    (minct-1, maxct-1, targetc, pwd)
}

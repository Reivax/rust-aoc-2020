pub fn main() {
    rustily_2()
}

fn rustily_1() {
    let input = include_str!("input.txt");
    let values: Vec<u32> = input.lines().map(|line| line.parse().unwrap()).collect();
    let results = values
        .iter()
        .enumerate()
        .map(|(lidx, &l)| values[lidx..]
            .iter()
            .enumerate()
            .filter(move|(ridx, r)| l+**r == 2020)
            .map(move|(_, r)| println!("Match: {} {} : {}", l, *r, l * *r))
        );
}

fn rustily_2() {
    let input = include_str!("input.txt");
    let values: Vec<u32> = input.lines().map(|line| line.parse().unwrap()).collect();
    for (lidx, l) in values
        .iter()
        .enumerate()
    {
        for (ridx, r) in values[lidx..]
            .iter()
            .enumerate()
            .filter(|(ridx, r)| *l+**r == 2020)
        {
            println!("Match: {} {} : {}", *l, *r, *l * *r);
        }
    }
}

fn pythonily() {
    println!("Hello From Part 1");
    let input = include_str!("input.txt");
    let input = parse_input(input);
    // println!("Input: {}", input[0].to_string())
    for (lidx, l) in input.iter().enumerate() {
        for (ridx, r) in input[lidx..].iter().enumerate() {
            if l + r == 2020 {
                println!("Located pairs, {} {}: {}", l, r, l * r);
            }
        }
    }
}

fn parse_input(input: &str) -> Vec<u32> {
    input.lines().map(|line| line.parse().unwrap()).collect()
}

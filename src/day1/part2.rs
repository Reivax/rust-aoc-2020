pub fn main() {
    pythonily()
}

fn pythonily() {
    println!("Hello From Part 2");
    let input = include_str!("input.txt");
    let input = parse_input(input);
    for (lidx, l) in input.iter().enumerate() {
        for (cidx, c) in input[lidx..].iter().enumerate() {
            for (ridx, r) in input[(lidx+cidx)..].iter().enumerate() {
                if l + r + c== 2020 {
                    println!("Located triplets, {} {} {}: {}", l, c, r, l * c * r);
                }
            }
        }
    }
}

fn parse_input(input: &str) -> Vec<u32> {
    input.lines().map(|line| line.parse().unwrap()).collect()
}

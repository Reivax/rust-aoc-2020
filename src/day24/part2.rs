use std::collections::HashMap;

static WHITE:bool = true;
static BLACK:bool = false;

pub fn main() {
    assert_eq!(part2(include_str!("test.txt"), 0), 10);
    assert_eq!(part2(include_str!("test.txt"), 1), 15);
    assert_eq!(part2(include_str!("test.txt"), 2), 12);
    assert_eq!(part2(include_str!("test.txt"), 3), 25);
    assert_eq!(part2(include_str!("test.txt"), 100), 2208);
    assert_eq!(part2(include_str!("input.txt"), 0), 382); // part1
    assert_eq!(part2(include_str!("input.txt"), 100), 3964); // part 2
}

fn initialize_state(input: &str)
    -> HashMap<(isize, isize, isize), bool> {
    let mut state: HashMap<(isize, isize, isize), bool> = HashMap::new();
    for line in input.split("\n") {
        let mut coord = (0, 0, 0);
        let mut new_row: Vec<char> = line.chars().collect();
        while new_row.len() > 0 {
            let c: char = new_row.remove(0);
            if c == 'e' {
                coord = move_coord(coord, "e");
            } else if c == 'w' {
                coord = move_coord(coord, "w");
            } else if c == 's' {
                let next_c:char = new_row.remove(0);
                if next_c == 'w' {
                    coord = move_coord(coord, "sw");
                } else if next_c == 'e' {
                    coord = move_coord(coord, "se");
                } else {
                    panic!("Bad char {} after {}", next_c, c);
                }
            } else if c == 'n' {
                let next_c:char = new_row.remove(0);
                if next_c == 'w' {
                    coord = move_coord(coord, "nw");
                } else if next_c == 'e' {
                    coord = move_coord(coord, "ne");
                } else {
                    panic!("Bad char {} after {}", next_c, c);
                }
            } else {
                panic!("Bac char {}", c)
            }
        }
        let current_state:&bool = state.get(&coord).unwrap_or(&WHITE);
        state.insert(coord, !current_state);
    }
    state
}

fn move_coord(coord: (isize, isize, isize), movement: &str) -> (isize, isize, isize){
    let mut delta = (0,0,0);
    if movement == "e"{
        delta = (1, -1, 0);
    } else if movement == "w"{
        delta = (-1, 1, 0);
    } else if movement == "nw"{
        delta = (0, 1, -1);
    } else if movement == "se"{
        delta = (0, -1, 1);
    } else if movement == "ne"{
        delta = (1, 0, -1);
    } else if movement == "sw"{
        delta = (-1, 0, 1);
    } else {
        panic!("bad movement {}", movement)
    }
    let (x, y, z) = coord;
    let (dx, dy, dz) = delta;
    (x+dx, y+dy, z+dz)
}

fn tag_neighbors(
    neighbor_count_board: &mut HashMap<(isize, isize, isize), usize>,
    coord: &(isize, isize, isize)
) {
    let (x,y,z) = coord;
    for delta in vec![(1,-1,0), (-1,1,0), (1,0,-1), (-1,0,1), (0,1,-1),(0,-1,1)] {
        let (dx, dy, dz) = delta;
        *neighbor_count_board
            .entry((x+dx, y+dy, z+dz))
            .or_insert(0)
            += 1;
    }
}

fn advance_state(current_board: &HashMap<(isize, isize, isize), bool>)
                 -> HashMap<(isize, isize, isize), bool>
{
    let mut neighbor_count_board: HashMap<(isize, isize, isize), usize> = HashMap::new();
    let mut next_board:HashMap<(isize, isize, isize), bool> = HashMap::new();

    // For every active cell in the space, add one to every one of its neighbors.
    for coord in current_board.keys() {
        if *current_board.get(&coord).unwrap_or(&WHITE) == BLACK {
            tag_neighbors(&mut neighbor_count_board, &coord);
        }
    }
    // For every integer value in the neighbor map, check its value and its current state.
    for (coord, neighbor_count) in neighbor_count_board {
        let current_state = *current_board.get(&coord).unwrap_or(&WHITE);

        if current_state == BLACK && (neighbor_count == 0 || neighbor_count > 2){
            // next_board.insert(coord, WHITE);
        } else if current_state == WHITE && ( neighbor_count == 2 ) {
            next_board.insert(coord, BLACK);
        } else if current_state == BLACK {
            next_board.insert(coord, BLACK);
        }
    }
    next_board
}

fn part2(input: &str, turn_limit:usize) -> usize {
    println!("Turn Limit {}", turn_limit);
    let mut current_board = initialize_state(input);
    // quick_print_board(&current_board, 0);
    for generation in 1..=turn_limit as isize {
        println!("Generation: {} of {}", generation, turn_limit);
        current_board = advance_state(&current_board);
    }
    count_active(&current_board)
}

fn count_active(board: &HashMap<(isize, isize, isize), bool>) -> usize {
    let mut ctr:usize = 0;
    for (k, v) in board {
        if v == &BLACK {
            ctr += 1
        }
    }
    ctr
}

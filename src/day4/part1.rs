use std::collections::HashMap;

pub fn main() {
    test();
    effort_1();
}

fn test() {
    let input = include_str!("test.txt");
    assert_eq!(part_1(input), 2);
}

fn effort_1() {
    let input = include_str!("input.txt");
    assert_eq!(part_1(input), 182);
}

fn part_1(input: &str) -> usize{
    input
        .split("\n\n")
        .filter(|entry| assemble_dicts(entry))
        .count()
}

fn assemble_dicts(entry: &str) -> bool{
    //println!("Entry: {}", entry);
    let mut keys_dict = HashMap::new();
    let ct = entry
        .split_whitespace()
        .map(|kvp| {
            let kvp_split: Vec<&str> = kvp.split(':').collect();
            let k: String = kvp_split[0].chars().filter(|c| !c.is_whitespace()).collect();
            let v: String = kvp_split[1].chars().filter(|c| !c.is_whitespace()).collect();
            keys_dict.insert(k, v);
        })
        .count();
    //for (key, value) in &keys_dict {
    //    println!("{}: {}", key, value);
    //}
    //println!("Does it contain byr: {}", keys_dict.contains_key("byr"));
    does_dict_contain_all_entries(&keys_dict) &&
        are_dict_entries_well_written(&keys_dict)
}

fn does_dict_contain_all_entries(keys_dict: &HashMap::<String, String>) -> bool {
    let mandatory_fields = [
        "byr",
        "iyr",
        "eyr",
        "hgt",
        "hcl",
        "ecl",
        "pid",
        // "cid",
    ];
    mandatory_fields.iter().all(|key| keys_dict.contains_key(*key))
}

fn are_dict_entries_well_written(keys_dict: &HashMap::<String, String>) -> bool {
    let byr: &String = keys_dict.get("byr").unwrap();
    if !(1920..=2002).contains(&byr.parse().unwrap()){
        false
    }
    true
}

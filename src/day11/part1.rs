pub fn main() {
    assert_eq!(part1(load_file(include_str!("test.txt"))), 37);
    assert_eq!(part1(load_file(include_str!("input.txt"))), 2418);
}

static FLOOR:char = '.';
static OCCUPIED:char = '#';
static EMPTY:char = 'L';

fn load_file(input: &str) -> Vec<Vec<char>>{
    let mut board: Vec<Vec<char>> = vec![];
    //let mut board: Vec<Vec<char>> = Vec::new();
    for line in input.split("\n") {
        let new_row: Vec<char> = line.chars().collect();
        board.push(new_row);
    }
    board
}

fn print_board(board: &Vec<Vec<char>>) {
    for (rowct, row) in board.iter().enumerate() {
        print!("{} - ", rowct);
        for c in row {
            print!("{}", c);
        }
        println!("");
    }
}

fn is_cell_occupied(board: &Vec<Vec<char>>, row: i32, col:i32) -> bool {
    if row<0 || col<0 || row >= (board.len() as i32) || col >= (board[0].len() as i32) {
        false
    } else {
        board[row as usize][col as usize] == OCCUPIED
    }
}

fn count_neighbors(board: &Vec<Vec<char>>, row: usize, col:usize) -> usize {
    let row:i32 = row as i32;
    let col:i32 = col as i32;
    let mut neighbors: usize = 0;
    for rowprime in row-1..=row+1{
        for colprime in col-1..=col+1 {
            if rowprime == row && colprime == col {
                continue;
            } else if is_cell_occupied(board, rowprime, colprime) {
                if row == 3 && col == 0 {
                    println!("{}, {} occupied {}, {}", row, col, rowprime, colprime);
                }
                neighbors += 1;
            }
        }
    }
    neighbors
}

fn determine_state(board: &Vec<Vec<char>>, row: usize, col:usize) -> char {
    if board[row][col] == FLOOR {
        FLOOR
    } else if board[row][col] == EMPTY && count_neighbors(board, row, col) == 0 {
        OCCUPIED
    } else if board[row][col] == OCCUPIED && count_neighbors(board, row, col) >= 4 {
        EMPTY
    } else {
        board[row][col]
    }
}

//fn advance_board_state(current_board: &Vec<Vec<char>>) -> Vec<Vec<char>>{
fn advance_board_state(current_board: &Vec<Vec<char>>) -> Vec<Vec<char>> {
    let mut next_board: Vec<Vec<char>> = vec![];
    for row in 0..current_board.len() {
        // println!("New Row Check: {}", row);
        let mut next_row = vec![];
        for col in 0..current_board[row].len() {
            // println!("New Col Check: {}", col);
            let new_cell = determine_state(current_board, row, col);
            // println!("Converted: {} {} from {} to {}", row, col, current_board[row][col], new_cell);
            next_row.push(new_cell);
        }
        next_board.push(next_row);
    }
    next_board
}

fn count_seated(board: &Vec<Vec<char>>) -> i32 {
    board
        .iter()
        .map(|line| {
            line
                .iter()
                .filter(|&c| {
                    *c == OCCUPIED
                })
                .count() as i32
        })
        .sum::<i32>() as i32
}

fn equal_boards(l: &Vec<Vec<char>>, r: &Vec<Vec<char>>) -> bool {
    if l.len() != r.len() {
        return false
    }
    for row in 0..l.len() {
        if l[row].len() != r[row].len() {
            return false
        }
        for col in 0..l[row].len() {
            if l[row][col] != r[row][col]{
                return false
            }
        }
    }
    return true
}

fn part1(input_board: Vec<Vec<char>>) -> i32 {
    let mut current_board = input_board;
    let mut ctr: usize = 0;
    println!("CTR: {}", ctr);
    print_board(&current_board);
    loop {
        ctr += 1;
        let next_board = advance_board_state(&current_board);
        // advance_board_state(&current_board, next_board);
        println!("CTR: {}", ctr);
        // print_board(&next_board);
        if equal_boards(&next_board, &current_board){
            println!("THey match.");
            break;
        } else if ctr >= 90 {
            println!("Too many.");
            break
        } else {
            println!("Don't match.");
        }

        current_board = next_board;
    }
    count_seated(&current_board)
}

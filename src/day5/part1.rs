pub fn main() {
    assert_eq!(part_1(), 858);
}

fn part_1() -> i32 {
    let input = include_str!("input.txt");
    let max = input
        .lines()
        .map(|line| {
            let converted_line: String = line
                .chars()
                .enumerate()
                .map(|(idx, line)| match line {
                    'B' => '1',
                    'R' => '1',
                    'F' => '0',
                    'L' => '0',
                    _ => unreachable!(),
                }).collect();
            let my_int: i32 = i32::from_str_radix(&converted_line, 2).unwrap();
            // println!("Line: {} Converted {}  Parsed {}", line, converted_line, my_int);
            my_int
        })
        .max()
        .unwrap();
    println!("Max {}", max);
    max
}

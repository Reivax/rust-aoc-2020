use std::cmp;

pub fn main() {
    //assert_eq!(part_2(), 557);
    assert_eq!(part_2b(), 557);
}

fn part_2() -> i32 {
    let input = include_str!("input.txt");
    let mut all_ids:Vec<i32> = input
        .lines()
        .map(|line| {
            let converted_line: String = line
                .chars()
                .map(|(line)| match line {
                    'B'|'R' => '1',
                    'F'|'L' => '0',
                    _ => unreachable!(),
                }).collect();
            let my_int: i32 = i32::from_str_radix(&converted_line, 2).unwrap();
            // println!("Line: {} Converted {}  Parsed {}", line, converted_line, my_int);
            my_int
        })
        .collect();
    // all_ids.sort();
    let mut ret:i32 = 0;
    for id in (*all_ids.iter().min().unwrap())..(*all_ids.iter().max().unwrap()) {
        if !all_ids.contains(&id)
            // && all_ids.contains(&(&id-1))
            // && all_ids.contains(&(&id+1))
        {
            ret = id;
            break;
        }
    }
    println!("Missing seat: {}", ret);
    ret
}

fn part_2b() -> i32 {
    let input = include_str!("input.txt");
    let (min, max, sum) = input
        .lines()
        .fold((128*8, 0, 0),|(min, max, sum), line| {
            let my_int: i32 = line
                .chars()
                .fold(0, |acc, b| {
                    acc * 2 + match b {
                        'B' | 'R' => 1,
                        _ => 0
                    }
                });
            // println!("Line: {} Converted {}  Parsed {}", line, converted_line, my_int);
            (cmp::min(min, my_int), cmp::max(max, my_int), sum + my_int)
        });
    let mut ret:i32 = (min..=max).sum::<i32>()-sum;
    println!("Max: {}, Missing seat: {}", max, ret);
    ret
}

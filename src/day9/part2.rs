use std::collections::HashMap;
use std;

pub fn main() {
    assert_eq!(part2a(393911906), 59341885);
    assert_eq!(part2b(393911906), 59341885);
}

fn part2a(target: i32) -> (i32) {
    let mut window: Vec<i32> = vec![];
    include_str!("input.txt")
        .lines()
        .filter_map(|entry| {
            let i = entry.parse::<i32>().unwrap();
            window.insert(0, i);
            while window.iter().sum::<i32>() > target {
                window.pop();
            }
            if window.iter().sum::<i32>() == target {
                let summed: i32 = window.iter().min().unwrap() + window.iter().max().unwrap();
                println!("Found the condition: {}", summed);
                Some(summed)
            } else {
                None
            }
        }).next().unwrap()
}


fn sum(it: &Vec::<i32>) -> i32 {
    it.iter().sum::<i32>()
}

fn part2b(target: i32) -> (i32) {
    let mut window: Vec<i32> = vec![];
    for line in include_str!("input.txt").lines() {
        if sum(&window) < target {
            window.insert(0, line.parse::<i32>().unwrap());
        }
        while sum(&window) > target {
            window.pop();
        }
        if sum(&window) == target {
            return window.iter().min().unwrap() + window.iter().max().unwrap()
        }
    }
    return 0
}

use std::collections::HashMap;

pub fn main() {
    assert_eq!(part1_2(), (6686, 3476));
}

fn part1_2() -> (i32, i32) {
    let mut part1 = 0;
    let mut part2 = 0;
    include_str!("input.txt")
        .split("\n\n")
        .map(|entry| {
            let mut ct: i32 = 0;
            let mut dict = HashMap::with_capacity(26);
            entry
                .lines()
                .map(|line| {
                    ct = ct + 1;
                    line
                        .chars()
                        .map(|c| {
                            dict.insert(c, dict.get(&c).unwrap_or(&0) + 1);
                        })
                        .count(); //consume the iter.
                })
                .count(); //consume the iter.
            part1 += dict.len() as i32;
            part2 += dict
                .iter()
                .filter(|&(key, val)| {
                    val == &ct
                })
                .count() as i32;
        })
        .count(); //consume the iter.
    (part1, part2)
}
